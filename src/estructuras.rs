#[derive(Debug)]
pub struct Persona{
    pub edad: u32
}



#[derive(Debug)] // hace la estructura imprimible con {:?}
pub struct EstructuraImprimible {
    a: u32,
    b: u32,
    c: u32,
}


#[derive(Copy, Clone)] // permite pasar un estructura por copia
pub struct Humano {
    pub edad: u8,
    // peso: f64
    pub peso: Option<f64>,
}

impl Humano {
    // toda funcion que no tenga self como parametro inicial es considerada estatica
    fn new() -> Humano {
        return Humano {
            edad: 9,
            peso: None,
        };
    }
}

// es una coleccion de funciones
trait NuevoTrait {
    // como los traits son funciones que se agregan a los objetos deben llevar  &self

    fn mi_funcion(&self){
        println!("funcion de NuevoTrait");
    }

    fn otra_funcion(&self){
        println!("otra funcion")
    }

}

// basicamentes es como heredar la Interface,
// Humano : NuevoTrait
impl NuevoTrait for Humano{
    fn otra_funcion(&self){
        println!("funcion de NuevoTrait sobreescrita")
    }
}



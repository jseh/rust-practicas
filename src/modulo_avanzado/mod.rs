mod submodulo; // indico que 'submodulo' pertenece a este modulo 'modulo_avanzado'

pub mod modulo_de_un_archivo;
// lo hace publico para usarlo con 'use' entro modulo
// si este modulo solo va a ser usado por otros modulos pertenecientes
// a este modulo 'modulo_avanzado' no es necesario usar 'pub'
#ifndef _PRUEBA_LIB_H_
#define _PRUEBA_LIB_H_
 
int funA(int);
void prueba_con_punteros();
// la estrucura tiene que estar definida aqui para poder ser utilizada
struct Point 
{ 
   int x; 
   int y; 
};  

const int PI;


#endif
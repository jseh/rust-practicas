#include <stdio.h>
#include "libs/prueba_lib.h"

void fun(int a){
    printf("Value of a is %d\n", a);
}



void puntero_funcion(){
    // fun_ptr is a pointer to function fun()
    void (*fun_ptr)(int) = &fun;

    /* The above line is equivalent of following two
       void (*fun_ptr)(int);
       fun_ptr = &fun;
    */

    // Invoking fun() using fun_ptr
    (*fun_ptr)(10);

    return 0;
}

void puntero_doble(){
  int var = 789;

    // pointer for var
    int *ptr2;

    // double pointer for ptr2
    int **ptr1;

    // storing address of var in ptr2
    ptr2 = &var;

    // Storing address of ptr2 in ptr1
    ptr1 = &ptr2;

    // Displaying value of var using
    // both single and double pointers
    printf("Value of var = %d\n", var );
    printf("Value of var using single pointer = %d\n", *ptr2 );
    printf("Value of var using double pointer = %d\n", **ptr1);

}


void punteros(){
    int* ptr = (int*) malloc(100 * sizeof(int));



}

struct Book
{
    char name[10];
    int price;
}



//  gcc main.c libs/prueba_lib.c -o main && ./main
int main(){

    printf("todo correcto \n");
    int a = funA(20);
    printf("valor de a: %d \n", a);
    printf("valor de PI: %d \n", PI);

    struct Point p1 = {10, 30};
    printf("valor de p1.y: %d \n", p1.y);

    char *str2 = "Hello";

    char *str;
    str = "hello";  // Notice that str is pointer to the string, it is also name of the string. Therefore we do not need to use indirection operator *.



    //  array de  punteros
    char *name[3] = { 
        "Adam",
        "chris",
        "Deniel"
    };





    int i;
    int a[5] = {1, 2, 3, 4, 5};
    int *p = a;     // same as int*p = &a[0]
    for (i = 0; i < 5; i++)
    {
        printf("%d", *p);
        p++;
    }





    struct Book a;      //Single structure variable
    struct Book* ptr;   //Pointer of Structure type
    ptr = &a;
 
    struct Book b[10];  //Array of structure variables
    struct Book* p;     //Pointer of Structure type
    p = &b;  
}
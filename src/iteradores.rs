fn probar_iteradores(){
    let sentence = "This is a sentence in Rust.";
    let words: Vec<&str> = sentence
        .split_whitespace()
        .collect();
    let words_containing_i: Vec<&str> = words
        .into_iter()
        .filter(|word| word.contains("i"))
        .collect();
    println!("{:?}", words_containing_i);

    let numbers_iterator = [0,2,3,4,5].iter();
    let sum = numbers_iterator.fold(0, |total, next| total + next);
}
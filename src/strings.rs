pub fn test_strings(a: &str, b: &'static str, c: String) {

    // &str son llamadas 'string slices'
    // usar &str si no se necesita own o mutar
    // String se aloja en el heap


    let dog = "hachiko";
    let hachi = &dog[0..5];

    println!("siuu");


    let hello = "Hello ".to_string();
    let world = "world!";

    let hello_world = hello + world;


    let helloS = "Hello ".to_string();
    let worldS = "world!".to_string();
    let world2S = "world!".to_string();

    let hello_worldS = helloS + &worldS + &world2S;



    let hachiko = "忠犬ハチ公";

    for b in hachiko.as_bytes() {
        print!("{}, ", b);
    }

    println!("");

    for c in hachiko.chars() {
        print!("{}, ", c);
    }

    println!("");

    // las cadenas no estan indexadas
    let dog: Option<char> = hachiko.chars().nth(1); // kinda like hachiko[1]

}
use std::collections::HashMap;
use std::iter::Iterator;

fn tuplas(){
    // TUPLAS
    let tup = (10 , true, 7.8);
    println!("mi tupla: {} {} {}",tup.0,tup.1,tup.2);
    println!("mi tupla: {:?}", tup);

    // para crear multiples variables de una tupla
    let (v1, v2, v3) = tup;
    println!("v1: {}", v1);
}


fn slices(){
    // arrays slices
    let ar = [1, 2, 3, 4, 5];
    let slice = &ar[1..3]; // es una referencia inmutable a una porcion de la coleccion


    // string slices
    let ar2 = "Jose Manuel".to_string();
    let slice2: &str = &ar2[5..];
    println!("{:?}", slice2);


    // vector slices
    let a = vec![1, 2, 3, 4, 5];
    println!("{:?}", &a[1..=3]);
}


pub fn colecciones(){

    // tuples
    let tup = (1, 2.3, false);

    // hashmaps
    let mut contacts = HashMap::new();
    contacts.insert("Daniel", "798-1364");
    contacts.insert("Ashley", "645-7689");
    contacts.insert("Katie", "435-8291");
    contacts.insert("Robert", "956-1745");

    // crear vectores
    let a: Vec<i32> = Vec::new();
    let mut b = vec![1, 2, 3];
    b.push(5);
    println!("Imprimiendo Vector: {:?}",b);

    let t3: &i32 = &b[2];
    println!("El tercer elemento es: {}", t3);

    // crear arrays
    let arr = [1,3,4];
    let arr2: [bool; 2] = [true, false];
    let arr1: [i32; 500] = [0; 500];


    for n in arr.iter(){
        println!("{}", n)
    }

    recibirArray(&arr);
    recibirArray(&arr1[1 .. 4]);
}

pub fn recibirFuncion(miFuncion: &dyn Fn(i32) -> i32){

}

pub fn recibirArray(arr: &[i32]) {
}


pub fn retornarTupla(value: i32) -> (i32, i32) {
    return (20,40);
}


pub fn pruebas(){

    let sentence = "This is a sentence in Rust.";
    let words: Vec<&str> = sentence
        .split_whitespace()
        .collect();
    let words_containing_i: Vec<&str> = words
        .into_iter()
        .filter(|word| word.contains("i"))
        .collect();
    println!("{:?}", words_containing_i);

    let numbers_iterator = [0,2,3,4,5].iter();
    let sum = numbers_iterator.fold(0, |total, next| total + next);

    let (a, b) = retornarTupla(1);
    println!("tupla:  {:?}", (a+b));

    let tuple =  (1, "Hello");
    println!("First element: {}, second element: {}", tuple.0, tuple.1);

    // lambdas
    let closure_annotated = |i: i32| -> i32 { i + 1 };

    // tipos basicos
    let cc: u8;
    cc = 2;

    // slices

    // probando funciones
    // fun_test(5, &times2);
    // fun_test(5, &|i: i32| -> i32 { i + 1 });

}
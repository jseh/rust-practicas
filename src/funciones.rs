// Closures are a combination of a function pointer (fn) and a context.
// A closure with no context is just a function pointer.
// A closure which has an immutable context belongs to Fn.
// A closure which has a mutable context belongs to FnMut.
// A closure that owns its context belongs to FnOnce.

pub fn fun2(
    fnA: fn(u32, u32)->i32,
    fnB: &dyn Fn(u32, u32)->i32,
    fnC: &dyn FnMut(u32, u32)->i32,
    fnD: &dyn FnOnce(u32, u32)->i32,
){}

pub fn fun1(
    x: &str, // ref a string like
    y: &'static str,
    z: &'static mut str,
    a: &[u32], // ref a array u32
    aa: &mut [u32],
    aaa: Box<[u32]>,
    aaaa: [u32; 4], // array con limite
    av: Vec<u32>, // vector
    b: &mut u32,
    c: &u32,
    d: u32,
){}

pub fn traits(
    o: Box<&dyn NuevoTrait>, // caja para referencia a cualquier objeto que implemente el trait
    o2: Box<dyn NuevoTrait>, // caja para cualquier objeto que implemente el trait
    o3: &dyn NuevoTrait, //  referencia de solo lectura a trait
    o4: &mut dyn NuevoTrait // referencia lectura-escritura a trait
){
    let x: Box<&dyn NuevoTrait> = Box::new(&Humano::new()); // caja a referencia del tipo del trait
    println!("todo bien con los traits");
}

pub fn constantes(){
    // constante vs variable inmutable
    const CONS: u32 = 23;
    let cons: u32 = 20;

}

// modulos que pertenecen a este crate
mod threads_pruebas;
mod arrays;
mod estructuras;
mod modulo_avanzado;
mod errores;

// los imports propiamente
use threads_pruebas::prueba_de_arc;
// usar 'use threads_pruebas::algo;' si se esta desde main.rs
// usar 'use crate::algo;' si se esta desde otro archivo
// usar 'use nombre_paquete::algo;' si viene desde una paquete

use arrays::probar_alteracion_de_array;
use estructuras::Persona;
use modulo_avanzado::modulo_de_un_archivo::hola;
use errores::{mostrar_error};


fn probar_condicionales(){
    if 20 == 20 && true {
        println!("es true");
    }else{
        println!("es false");
    }

    let variableAEvaluar = 20;

    match variableAEvaluar {
        20 => {println!("ok: {}", variableAEvaluar)}
        _ => {println!("default")}
    }
}

fn probar_evaluaciones_matematicas(){

    // de izq a derecha
    // si trabajo solo con enteros me lo redondeara
    // agregar el .O para obtener el verdarero resultado
    // 2 + (3 * 4) - (3 / 20)
    let a : f64 = 2.0 + 3.0 * 4.0 - 3.0 / 20.0;
    println!("a: {}", a );
}

fn probar_referencias(){
    let n = 5;
    let m = &n;
    println!("{:?}", n );
    println!("{:?}", m );

    let l = (n * *m) + (*m * n);
    println!("{:?}", l );
}


fn main() {
    println!("ok");
}

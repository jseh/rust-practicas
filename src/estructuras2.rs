#[derive(Debug)]
pub struct Humano {
    nombre: String,
    edad: i32,
}

impl Humano {
    // 'Self' representa a la estructura
    fn new() -> Self {
        return Self {
            nombre: "".to_string(),
            edad: 0,
        };
    }

    // &mut self es un 'this' de lectura y escritura
    fn set_nombre(&mut self, nombre: String) -> &mut Self {
        self.nombre = nombre;
        return self;
    }

    // '&mut Self' porque lo que se va retornar es un 'this mutable'
    fn set_edad(&mut self, edad: i32) -> &mut Self {
        self.edad = edad;
        return self;
    }

    // &self es un 'this' de solo lectura
    fn mostrar_nombre(&self) {
        println!("{:?}", self.nombre);
    }

    fn mostrar_edad(&self) {
        println!("{:?}", self.edad);
    }

    fn recibir(&self, mref: &mut i32 ){
        *mref = *mref + 4000;
        println!("{}", *mref);
    }
}

fn probar_patron_builder(){

    let mut lola = Humano::new();
    let mut lola2 = Box::new(Humano::new());
    // lola.set_nombre("maria".to_string());
    // lola.set_edad(26);
    lola.set_nombre("maria".to_string()).set_edad(27);

    lola.mostrar_nombre();
    lola.mostrar_edad();

    lola2.set_edad(25);
    lola2.mostrar_edad();

    // mutar valor usando referencia
    let mut mivalor = 200;
    lola2.recibir(&mut mivalor); // '&mut var' significa entregale una referencia mutable de 'mivalor'
    println!("{:?}", mivalor );
}
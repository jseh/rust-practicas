fn test_lifetime(){

    const CONST_MEEP: &str = "MEEP";
    const CONST_LIFETIME_MEEP: &'static str = "MEEP";

    static STATIC_MEEP: &'static str = "MEEP";
    static STATIC_LIFETIME_MEEP: &str = "MEEP";


    // HAY DOS FORMAS DE DEFINIR ALGO ESTATICO(una para constantes y otra para strings)
    // se guarda en el binario, y esta diponible durante toda la ejecucion del programa
    // deberian ser de solo lectura

    // 'static es el tiempo de vida as largo del programa
    // su tiempo de vida solo puede ser cambiado a uno mas corto
    let  sss:&'static str = "asdf"; //  existe

    // Make a constant with `'static` lifetime.
    static NUM: i32 = 18;
}


pub fn scopes(){
    let v = 2;
    //shadowing
    let v = 'v';

    // This binding lives in the main function
    let long_lived_binding = 1;

    // This is a block, and has a smaller scope than the main function
    {
        // This binding only exists in this block
        let short_lived_binding = 2;

        println!("inner short: {}", short_lived_binding);

        // This binding *shadows* the outer one
        let long_lived_binding = 5_f32;

        println!("inner long: {}", long_lived_binding);
    }
    // End of the block

    // Error! `short_lived_binding` doesn't exist in this scope
    // println!("outer short: {}", short_lived_binding);
    // FIXME ^ Comment out this line

    println!("outer long: {}", long_lived_binding);

    // This binding also *shadows* the previous binding
    let long_lived_binding = 'a';

    println!("outer long: {}", long_lived_binding);

    println!("outer long: {}", v);
}


fn get_ref<'x>(p: &'x i32, pp: &'x i32) -> &'x i32 {
    println!("{:?}", p);
    if p > pp {
        p
    } else {
        pp
    }
}


fn probar_contrar(){
    // fn t3<'x, 'b>() -> &'b u32{
    //     let n = 8;
    //     &n
    // }
}
// enumeracion normal
enum Foo {
    Bar,            // 0
    Baz = 123,      // 123
    Quux,           // 124
}

// para mostrar la enumareacion usar {:?}
#[derive(Debug)]
enum Direccion{
    Norte, // es una variable de tipo 'int' y con valor 1
    Sur = 2,
    Este = 3,
    Oeste = 4,
}

// R y E definen los tipos que pueden usar cada una de la enumeraciones
// TodoBien(R) es una variables de tipo
#[derive(Debug)]
enum Resultado<R, E>{
    // puede guardar un valor, de un determinado tipo
    // son tuplas?¿ investigar
    TodoBien(R),
    Fallo(E) // es una variable llamada 'Falla' de tipo E,
}

fn test_match(enumeracion: Resultado<i32, bool>){

    match enumeracion {
        Resultado::TodoBien(respuesta) => println!("Todo fue bien, el resultado es: {} ", respuesta),
        Resultado::Fallo(error) => println!("Hubo un error: {} ", error),
    }

}

fn test_match2(mi_dir: Direccion){

    match mi_dir {
        Direccion::Norte => println!("Opcion Norte"),
        Direccion::Sur => println!("Opcion Sur"),
        Direccion::Este => println!("Opcion Este"),
        Direccion::Oeste => println!("Opcion Oeste"),
        // _ => println!("no encontrado") // poner un default si no se cubren todos lo casos del enum
    }

}

pub fn test_enums(){
    let res: Resultado<i32, bool> = Resultado::TodoBien(10);
    let res2: Resultado<i32, bool> = Resultado::Fallo(true); // se inicializa con un valor

    test_match(res);
    test_match(res2);


    let res3: Direccion = Direccion::Norte;
    test_match2(res3);

    // es posible obtner en el valor de una enumaracion normal
    let baz_discriminant = Foo::Quux as u32;
    println!("{}", baz_discriminant);

}
use std::sync::Arc;
use std::thread;
use std::time::Duration;


pub fn prueba_de_arc(){
    let mut com = Arc::new("Jose Manuel".to_string());

    let t1 = {
        let mut com = com.clone();

        // se devuelve la ultima linea como el return
        thread::spawn(
            move || {
                com = Arc::from("ok".to_string());
                println!("thread 1 {}", com );
            }
        )

    };

    let t2 = {
        let com = com.clone();

        thread::spawn(
            move || {
                println!("thread 2 {}", com );
            }
        )

    };

    t1.join().expect(" error t1");
    t2.join().expect(" error t2");
}


pub fn probar_arc(){

    // The Atomic Reference Counter (Arc) type is a smart pointer that lets you share
    // immutable data across threads in a thread-safe way.

    // The reason for needing the Arc type when attempting to share data across threads
    // is to ensure that the lifetime of the type that is being shared, lives as long as
    // the longest lasting thread.

    let foo = vec![0]; // creation of foo here
    // thread::spawn(|| {
    //     thread::sleep(Duration::from_millis(20));
    //     println!("{:?}", &foo);
    // });

}
use crate::estructuras::Persona;

fn alterar_array_mediante_referencia(
    arr: &mut [i32], // permite cambiar elemntos del array,  pero no reasignarlo completamente
    mut arr_reasignable: &mut [i32] // permite reasignar array completamente
){
    let a = [2,4,5];
    arr[1] = 10; // no puede ser reasignado
    // arr_reasignable = &a; // puede ser reasignado
}

// si es por copia o por movimiento depende de si es estructura o no (depende del tipo que maneja el array)
// ocurre movimiento con referencias mutables, (pero el prestamo se devuelve automaticamente)
fn alterar_array_mediante_movimiento(
    mut arr: [i32; 3] // cuando se pasa por movimiento usar mut para modificar los elementos del array
)  {
    arr[0] = 4; // ahora si se puede modificar
    println!("{:?}", arr);

}

// movimento ocurre  incluso para structuas en el heap y en el stack
fn alterar_array_mediante_movimiento2(
    mut arr: [Persona; 2]
)  {
    arr[0] = Persona{edad: 23};
}

// movimento ocurre ya sea que este en el heap o en el stack, porque lo envuelves en Box que es un estructura
fn alterar_array_mediante_movimiento3(mut arr: Box<[Persona; 2]>)  {
    arr[0] = Persona{edad: 23};
}


pub fn probar_alteracion_de_array(){

    let mut x =[1,6,9];
    let mut z =[2,12,81];
    alterar_array_mediante_referencia(&mut x, &mut z);
    println!("{:?}", x );
    println!("{:?}", z );

    let a =[3,3,7];
    let b = [Persona{edad: 10}, Persona{ edad: 20}];
    let c= Box::new([Persona{edad: 10}, Persona{ edad: 20}]);
    alterar_array_mediante_movimiento(a); // se pasa por copia en realidad, porque el tipo es i32
    println!("{:?}", a);

    alterar_array_mediante_movimiento2(b); // se pasa por movimiento, porque el tipo es 'Persona'
    // println!("{:?}", b); // esta linea es inalcanzable por el prestamo

    alterar_array_mediante_movimiento3(c);
    // println!("{:?}", c); // esta linea es inalcanzable por el prestamo

}

fn swap_array<'x>(mut a: &'x[i32], mut b: &'x[i32]){
    let c = b;
    b = a;
    a = c;

    println!("{:?}", a);
    println!("{:?}", b);

}

fn swap_array2<'x>(mut a: [i32;3], mut b: [i32;3]){
    let c = b;
    b = a;
    a = c;

    println!("{:?}", a);
    println!("{:?}", b);

}

fn swap_array3<'x>(mut a: &mut[i32;3], mut b: &mut[i32;3]){
    let c = *b;
    *b = *a;
    *a = c;

    println!("{:?}", a);
    println!("{:?}", b);

}
